/*State Box Class*/
class StateBoxClass
{
public:
	StateBoxClass();
	~StateBoxClass();
	int canCount1;
	int cntPer1;
	int outCount1;
	int lengthOneCodesIterator1;
	double elapsedTime;
	double elapsedTime1;
	void update();
	void resume();
private:

};

StateBoxClass::StateBoxClass()
{
	elapsedTime = 0;
}

StateBoxClass::~StateBoxClass()
{

}

void StateBoxClass::update(){
	//Record current status
	canCount1 =  canCount;
	cntPer1 = cntPer;
	outCount1 = outCount;
	lengthOneCodesIterator1 = lengthOneCodesIterator;
	elapsedTime1 = elapsedTime;
	int tempstop_s=clock();
	elapsedTime1 += (tempstop_s-start_s)/double(CLOCKS_PER_SEC)*1000;
	//write to file
	FILE *stateBoxFile;
	errno_t error	= fopen_s(&stateBoxFile, "StateBox.txt","w");
	while(error!=0){
		cout<<"Stucked"<<endl;
		printf("%d %d %d %d %lf\n",lengthOneCodesIterator1, canCount1,cntPer1,outCount1,elapsedTime1);
		error	= fopen_s(&stateBoxFile, "StateBox.txt","w");
	}
	fprintf_s(stateBoxFile,"%d %d %d %d %lf",lengthOneCodesIterator1, canCount1,cntPer1,outCount1,elapsedTime1);
	fclose(stateBoxFile);
	//close output file
	fclose(fpOutput);
	//re-open output file in append mode
	error	= fopen_s(&fpOutput, pathout, "a");
	if(error!=0){
		cout<<"ERROR::Output cannot be re-opened. Please ensure path is not restricted"<<endl;
		cout<<path<<endl;
		return;
	}

}

void StateBoxClass::resume(){
	//Read stateBoxObject from file
	FILE *stateBoxFile;
	errno_t error	= fopen_s(&stateBoxFile, "StateBox.txt","r");
	fscanf_s(stateBoxFile,"%d %d %d %d %lf",&lengthOneCodesIterator1, &canCount1,&cntPer1,&outCount1,&elapsedTime);
	fclose(stateBoxFile);
}