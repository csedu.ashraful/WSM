#include "LIBHEADER.h"
#include "Header.h" 
#include "GLOBALVARIABLES.h"
#include "Functions.h"
#include "wsm.h"
#include "StateBox.h"
/**Note: Header include sequence cannot be altered**/




int main(){
	StateBoxClass stateBoxObject;
	cout<<"Data preprocessing required?"<<endl;
	cout<<"press 1 to preprocess\nPress 2 to resume"<<endl;
	int choice;
	cin>>choice;
	cin.getline(filename, 100);
	if(choice==1){
		srand(time(NULL));
		cout<<"Enter Cancerdata foldername: ";		
		cin.getline(filename, 100);
		strcat_s(preproPath, 1000, filename);
		strcat_s(preproPathout, 1000, filename);
		preprocessData(preproPath,preproPathout);
	}else if(choice==2){
		//Resume State Box from File
		stateBoxObject.resume();

	}
	cout<<"Enter filename from dataset path: ";
	cin.getline(filename, 100);
	strcat_s(path, 1000, filename);
	strcat_s(pathout, 1000, filename);
	cout<<"Enter weighted support threshold: ";
	cin>>threshold;

	
	errno_t err;
	err	= fopen_s(&fp, path, "r");
	if(err!=0){
		cout<<"ERROR::Dataset cannot be opened. Please ensure path is correct"<<endl;
		cout<<path<<endl;
		return 0;
	}
	//Modified for State save
	if(choice==2) err	= fopen_s(&fpOutput, pathout, "a");
	else err	= fopen_s(&fpOutput, pathout, "w");

	if(err!=0){
		cout<<"ERROR::Output cannot be opened. Please ensure path is not restricted"<<endl;
		cout<<path<<endl;
		return 0;
	}
	start_s=clock();
	//saving position of first graph
	fgetpos(fp, &pos);
	positions.push_back(pos);
	
	//Setting empty tuple for EdgeClass
	EdgeClass.t.disu = -1;
	EdgeClass.t.disv = -1;

	//reading first graph
	gfp_pair temp = readNextGraph(fp);
	while(temp.g.vc != 0){ //loop breaking when returned graph has zero vertex
		
		//printing graph
		//temp.g.print();
		cout<<".";
		totalGraphsInDatabase++;
		//saving position of next graph in vector
		fgetpos(fp, &pos);
		positions.push_back(pos);
		

		//////////////////perform WSM initial operations::Edge Class Assignment and LengthOneCode Generation/////////////////////
		/************************************************************************************************************************
		Points to be noted: edge class assignment is unnecessary and 
		therefore (can be)will be avoided in final version
		************************************************************************************************************************/

		//update EdgeClass
		//EdgeClass.OLvec[temp.g.ec].ec = temp.g.ec;
		EdgeClass.OLMvec[temp.g.ec].gids.push_back(temp.g.id);

		//update maxEdge
		if(temp.g.ec>maxEdge) maxEdge = temp.g.ec;
		if(temp.g.ec<minEdge) minEdge = temp.g.ec;
		vector<EdgeMap> em;
		for(unsigned int i = 0; i<temp.g.edge.size(); i++){
			Arc edg = temp.g.edge[i];
			//check if it is the first occurrence of edg in this graph
			bool first = true;
			for(unsigned int j = 0; j<em.size(); j++){
				if((em[j].labeluv == edg.glabel && em[j].labelu == edg.u.nlabel && em[j].labelv == edg.v.nlabel)||(em[j].labeluv == edg.glabel && em[j].labelu == edg.v.nlabel && em[j].labelv == edg.u.nlabel)){
					first = false;
					break;
				}
			}
			if(first){
				EdgeMap edgMap;
				if(edg.u.nlabel.compare(edg.v.nlabel)<0){
					edgMap.labelu = edg.u.nlabel;
					edgMap.labelv = edg.v.nlabel;
				}else{
					edgMap.labelv = edg.u.nlabel;
					edgMap.labelu = edg.v.nlabel;
				}
				edgMap.labeluv = edg.glabel;
				em.push_back(edgMap);
				unsigned int edgIndex = setEdgeIndex(edgMap.labelu, edgMap.labelv, edgMap.labeluv);
				if(lengthOneCodes.size() <= edgIndex){ //new edge so enlist in lengthone codes
					extendedTuple t;
					t.disu = 0;
					t.disv = 1;
					t.labelu = edgMap.labelu;
					t.labelv = edgMap.labelv;
					t.labeluv = edgMap.labeluv;
					t.weight = edg.weight;

					//update maxW
					if(t.weight>maxW) maxW = t.weight;
					
					Code code;
					code.tuples.push_back(t);
					code.OLMvec[temp.g.ec].gids.push_back(temp.g.id);

					lengthOneCodes.push_back(code);

				}else{
					lengthOneCodes[edgIndex].OLMvec[temp.g.ec].gids.push_back(temp.g.id);
				}
			}
		}

		//reading next graph 
		temp = readNextGraph(fp);
	}

	cout<<endl;
	//initializeStoredIndicesArray();
	//Resume......
	if(choice==2){
		lengthOneCodesIterator = stateBoxObject.lengthOneCodesIterator1;
		canCount = stateBoxObject.canCount1;
		cntPer = stateBoxObject.cntPer1;
		outCount = stateBoxObject.outCount1;
		start_s=clock();
	}
	for(; lengthOneCodesIterator<lengthOneCodes.size(); lengthOneCodesIterator++ ){
		//Save stateBox
		stateBoxObject.update();
		if(MaxPws(lengthOneCodes[lengthOneCodesIterator]).mws>=threshold){
			canCount++;
			performWSM(lengthOneCodes[lengthOneCodesIterator]);
		}
	}


	stop_s=clock();
	double tm = (stop_s-start_s)/double(CLOCKS_PER_SEC)*1000;
	tm += stateBoxObject.elapsedTime;
	FILE *ff;
	err	= fopen_s(&ff, "summary.txt","a");
	fprintf_s(ff,"%s \nThreshold %lf\ntime %lf\nFWS %d\nCandidate %d\ngenerated g.count %d\ntotal g in db %d\n",filename,threshold, tm, outCount, canCount, cntPer, totalGraphsInDatabase);
	fprintf(ff,".................................................................................................\n");
	fclose(ff);

	cout<<"Threshold: "<<threshold<<endl;
	cout << "time: " << tm << endl;
	cout<<"\nMining done. "<< outCount <<" frequent subgraphs found"<<endl;
	cout<<"Candidate Count:"<<canCount<<endl;
	cout<<totalGraphsInDatabase<<endl;
	fclose(fpOutput);
	fclose(fp);
	return 0;
}