#define MAXLENGTH 999999
#define MAXEDGE 1000
#define MAXNODE 1000
#define INVALID -1
#define MAXSTOREDGRAPH 600
#define MAXGRAPHINDATABASE 10000
int outCount=0;
FILE *fpOutput;

struct Node{
	int nid;
	string nlabel;
};

struct Arc{
	Node u, v;
	double weight;
	string glabel;
};

class Graph
{
public:
	Graph();
	~Graph();
	void print();
	int vc,ec,id;
	vector<Node>vertex;
	vector<Arc>edge;
	Node getNode(int nid);
	//vector<Node> getNeigbors();
	
private:

};

Graph::Graph()
{
	vertex.reserve(MAXNODE);
	edge.reserve(MAXEDGE);
}

Graph::~Graph()
{
}

void  Graph::print(){
		cout<<id<<" : "<<"("<<vc<<","<<ec<<")"<<endl;
		cout<<"Nodes:"<<endl;
		for(unsigned int i = 0; i< vertex.size(); i++){
			cout<<"			("<<vertex[i].nid<<","<<vertex[i].nlabel<<")"<<endl;
		}
		cout<<"Edges:"<<endl;
		for(unsigned int i=0; i<edge.size(); i++){
			cout<<"        <"<<edge[i].u.nid<<","<<edge[i].v.nid<<","<<edge[i].u.nlabel<<","<<edge[i].v.nlabel<<","<<edge[i].glabel<<","<<edge[i].weight<<">"<<endl;
		}

}

Node Graph::getNode(int nid){
		//cout<<"aka ..getNode"<<nid<<endl;
		for(unsigned int i = 0; i<vertex.size(); i++){
			if(vertex[i].nid == nid) return vertex[i];
		}
		//cout<<"nid "<<nid<< "not found"<<endl;
			print();
			return Node();
	}


struct gfp_pair{
	Graph g;
	FILE *fp;
};

struct extendedTuple{
	int disu, disv;
	string labelu,labelv, labeluv;
	double weight;
};

struct OLM{		//occurrence list member
	//int ec;		//number edges of each graph in gids
	vector<int>gids;
};


//Occurrence List
class OL
{
public:
	OL();
	~OL();
	extendedTuple t;
	OLM OLMvec[MAXEDGE+1];
	void print();
private:

};

OL::OL()
{

}

OL::~OL()
{
}


void printTuple(extendedTuple t){
	cout<<"<"<<t.disu<<", "<<t.disv<<", "<<t.labelu<<", "<<t.labelv<<", "<<t.labeluv<<", "<<t.weight<<">"<<endl;
}
void OL::print(){
	printTuple(t);
	for(unsigned int i = 0; i<MAXEDGE; i++){
		if(OLMvec[i].gids.size()>0){
			cout<<"Edge Count = "<<i<<endl;
			for(unsigned j = 0; j<OLMvec[i].gids.size(); j++){
				cout<<OLMvec[i].gids[j]<<" ";
			}
			cout<<endl;
		}

	}
}

class Code
{
public:
	Code();
	~Code();
	vector<extendedTuple>tuples;
	OLM OLMvec[MAXEDGE+1];
	void print();
	void writeCode();

private:

};

Code::Code()
{
	tuples.reserve(MAXEDGE);
}

Code::~Code()
{
}


void Code::writeCode(){
	fprintf_s(fpOutput, "Code %d\n",++outCount);
	//fclose(fpOutput);
	for(unsigned int i = 0; i<tuples.size(); i++){
		fprintf_s(fpOutput,"< %d, %d, %s, %s, %s, %.2lf>\n",tuples[i].disu, tuples[i].disv, tuples[i].labelu.c_str(), tuples[i].labelv.c_str(),tuples[i].labeluv.c_str(), tuples[i].weight);
	}
}

void Code::print(){
	for(unsigned int i = 0; i<tuples.size(); i++){
		printTuple(tuples[i]);
	}
	/*for(unsigned int i = 0; i<MAXEDGE; i++){
		if(OLMvec[i].gids.size()>0){
			cout<<"Edge Count = "<<i<<endl;
			for(unsigned j = 0; j<OLMvec[i].gids.size(); j++){
				cout<<OLMvec[i].gids[j]<<" ";
			}
			cout<<endl;
		}

	}*/
}

struct EdgeMap
{
	string labelu;
	string labelv;
	string labeluv;
	//double mappedVal; // weight
};



//Copied from gSpan project

struct phi{
	vector<int>vertices;
	phi(){
		vertices.reserve(MAXNODE);
	}
};

struct PHI{
	vector<phi>phis;
	PHI(){
		phis.reserve(MAXEDGE);
	}
};

struct Neighbor{
	vector<Node>vertices;
	vector<string>labeluvs;
	Neighbor(){
		vertices.reserve(MAXNODE);
	}
};


struct sup_mws_pair
{
	double mws;
	double ws;
	double mxW;
};


struct edge_freq{
	Arc e;
	int freq;
};




struct graphs_and_time{
	Graph g;
	int accessTime;
};