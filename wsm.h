/********************************************************************************************************************

*********************************Declaration of Functions in this file***********************************************

*********************************************************************************************************************/
void performWSM(Code C);
vector<OL> getAllExtensions(Code C);
vector<OL> getAllExtensionsWithG(Code C, Graph G);
vector<Node> getRightMostNodes(Code C);
bool isForwardEdge(extendedTuple t);
void printPHI(PHI varPHI);
PHI SubgraphIsomorphisms(Code C, Graph G);
PHI initializePHI(Code C, Graph G);
Neighbor getNeighbors(Graph G,int u);
bool isPhiInv(int x,phi varphi);
int getPhiInv(int x,phi varphi);
bool existNode(int v,vector<Node>R);
bool existEdge(int ur,int v,Code C);
double getWeight(string labelu, string labelv,string labeluv, Graph G);
int getOLtIndex(vector<OL>OLvec, extendedTuple t);
int compareTuple(extendedTuple t1, extendedTuple t2);
bool gidExist(OLM olm, int gid);
bool isCanonical(Code C);
Graph getGraphFromCode(Code C);


/********************************************************************************************************************

*********************************Implementation of Functions in this file********************************************

*********************************************************************************************************************/
void performWSM(Code C){
	vector<OL>OLvec;
	OLvec = getAllExtensions(C);
	for(unsigned int i = 0; i<OLvec.size(); i++){
		//OLvec[i].print();
		Code  C_prime = Code(C);
		C_prime.tuples.push_back(OLvec[i].t);
		//C_prime.OLMvec = OLvec[i].OLMvec;
		for(int j = minEdge; j<=maxEdge ;j++){
			C_prime.OLMvec[j] = OLvec[i].OLMvec[j];
		}
		cout<<"code "<<++cntPer<<endl;
		C_prime.print();
		sup_mws_pair mxpws = MaxPws(C_prime);
		cout<<mxpws.ws<<"-----"<<mxpws.mws<<endl;
		
		if(mxpws.ws>=threshold){
			canCount++;
			/*if(C_prime.tuples.size()==5){
				int k = 4;
			}*/
			if(isCanonical(C_prime)){
				cout<<"                                       FWS"<<endl;
				//print or save output
				//C_prime.print();
				C_prime.writeCode();
				//recursive call
				performWSM(C_prime);
			}
			else cout<<"                                      not Canonical"<<endl;
		}else if(mxpws.mws >= threshold){
			canCount++;
			if(isCanonical(C_prime)){
				/*if(mxpws.ws>mxpws.mws){
					C_prime.print();
				}*/
				cout<<"                                      Candidate"<<endl;
				performWSM(C_prime);
			}
			else cout<<"                                       not Canonical"<<endl;
		}else cout << "                                        maxPws<thr"<<endl;	
		//cout<<"Debug me2"<<endl;
	}
	//cout<<"ERROR::performWSM is not implemented"<<endl;
	return;
}



vector<OL> getAllExtensions(Code C){
	vector<OL>OLvec;
	vector<Node>R;
	R = getRightMostNodes(C);
	Node ur = R[0];
	if(C.tuples.size()==4){
		int tem = 10;
	}
	for(int i = minEdge; i<=maxEdge ; i++){
		for(unsigned int j = 0; j<C.OLMvec[i].gids.size(); j++){
			Graph G = readGraph( C.OLMvec[i].gids[j]);
			PHI phi_i = SubgraphIsomorphisms(C,G);
			for(unsigned int k = 0; k<phi_i.phis.size(); k++){
				phi smPhi = phi_i.phis[k];
				//Backward Edge Extension from rightmost child ur
				int phi_ur = smPhi.vertices[ur.nid];
				Neighbor nbr = getNeighbors(G,phi_ur);
				for(unsigned int l = 0; l<nbr.vertices.size(); l++){
					Node x = nbr.vertices[l];
					string labeluv = nbr.labeluvs[l];
					int v = getPhiInv(x.nid,smPhi);
					if(v!=-1 && existNode(v,R) && !existEdge(ur.nid,v,C)){
						extendedTuple t;
						t.disu = ur.nid;
						t.disv = v;
						t.labelu = ur.nlabel;
						t.labelv = x.nlabel;						
						t.labeluv = labeluv;
						t.weight = getWeight(t.labelu, t.labelv,t.labeluv, G);
						//18: Add pair (t;OLt) in OLvec if not added before
						int indx  = getOLtIndex(OLvec, t);
						if(indx==-1){ // ol does not exist
							indx = OLvec.size();
							OL ol;
							ol.t = t;
							OLvec.push_back(ol);
						}

						//19: Add or modify entry OLt(qi): OLt(qi) = OLt(qi) [ Gj
						if(!gidExist(OLvec[indx].OLMvec[G.ec], G.id)) OLvec[indx].OLMvec[G.ec].gids.push_back(G.id);

					}

				}

				//Forward edge extension from rightmost path R
				for(unsigned int l = 0; l<R.size(); l++){
					int phi_u = smPhi.vertices[R[l].nid];
					Neighbor nbr_u = getNeighbors(G,phi_u);
					for(unsigned int k = 0; k<nbr_u.vertices.size(); k++){
						Node x = nbr_u.vertices[k];
						string labeluv = nbr_u.labeluvs[k];
						if(!isPhiInv(x.nid, smPhi)){
							extendedTuple t;
							t.disu = R[l].nid;
							t.disv = ur.nid+1;
							t.labelu = R[l].nlabel;
							t.labelv = x.nlabel;
							t.labeluv = labeluv;
							t.weight = getWeight(t.labelu, t.labelv,t.labeluv, G);
							//23: Add pair (t;OLt) in OLvec if not added before
							int indx  = getOLtIndex(OLvec, t);
							if(indx==-1){ // ol does not exist
								indx = OLvec.size();
								OL ol;
								ol.t = t;
								OLvec.push_back(ol);
							}

							//19: Add or modify entry OLt(qi): OLt(qi) = OLt(qi) [ Gj
							if(!gidExist(OLvec[indx].OLMvec[G.ec], G.id))  OLvec[indx].OLMvec[G.ec].gids.push_back(G.id);
						}

					}
				}
			}
		}		
	}
	return OLvec;
}
//int countS = 0;
PHI SubgraphIsomorphisms(Code C, Graph G){

	/*fsetpos(fp, &positions[gid-1]);
	gfp_pair temp = readNextGraph(fp);*/
	cout<<"-";
	/*countS++;
	if(countS==4){
		int m = 90;
	}*/
	//Graph G = readGraph(gid);
	PHI varPHI = initializePHI(C,G); //not tested
	for(unsigned int i = 0; i<C.tuples.size();i++){
		extendedTuple t = C.tuples[i];
		PHI varPHI_prime;
		for(unsigned int j = 0; j<varPHI.phis.size(); j++){
			phi varphi = varPHI.phis[j];
			if(isForwardEdge(t)){
				//forward edge
				int phiu = varphi.vertices[t.disu];
				Neighbor Ngu = getNeighbors(G,phiu);
				for(unsigned int k=0; k<Ngu.vertices.size(); k++){
					int x = Ngu.vertices[k].nid;
					string labeluv = Ngu.labeluvs[k];
					if(!isPhiInv(x,varphi) && t.labelv == G.getNode(x).nlabel && labeluv == t.labeluv){//third condition not required
						phi varphi_prime;
						
						varphi_prime.vertices.assign(varphi.vertices.begin(),varphi.vertices.end());
						//varphi_prime = varphi;


						varphi_prime.vertices.push_back(x);
						varPHI_prime.phis.push_back(varphi_prime);
					}
				}
			}else{
				//backward edge
				int phiu = varphi.vertices[t.disu];
				Neighbor Ngu = getNeighbors(G,phiu);
				int phiv = varphi.vertices[t.disv];
				for(unsigned int k = 0; k<Ngu.vertices.size();k++){
					if(phiv == Ngu.vertices[k].nid){
						varPHI_prime.phis.push_back(varphi);
						break;
					}
				}

			}
		}
		varPHI = varPHI_prime;
	}
	//printPHI(varPHI);
	return varPHI;
}






bool isCanonical(Code C){
	//int dummy;
	Code canCode;
	Graph G = getGraphFromCode(C);
	C.OLMvec[G.ec].gids.push_back(1);
	for(unsigned int i = 0; i < C.tuples.size(); i++){
		vector<OL>OLvec = getAllExtensionsWithG(canCode, G);
		extendedTuple mint = OLvec[0].t; //Error Line
		for(unsigned int j = 1; j<OLvec.size(); j++){
			if(compareTuple(mint, OLvec[j].t)>0) mint = OLvec[j].t;
		}
		if(compareTuple(C.tuples[i], mint)>0) return false;
		canCode.tuples.push_back(mint);
	}
	return true;
}



/********************************************************************************************************************

*******************************************Helper Functions**********************************************************

*********************************************************************************************************************/


bool isPhiInv(int x,phi varphi){
	for(unsigned int i = 0; i< varphi.vertices.size();i++){
		if(varphi.vertices[i]==x) return true;
	}
	return false;
}

int getPhiInv(int x,phi varphi){
	for(unsigned int i = 0; i< varphi.vertices.size();i++){
		if(varphi.vertices[i]==x) return i;
	}
	return -1;
}



Neighbor getNeighbors(Graph G,int u){
	
	Neighbor ngu;
	for(unsigned int i = 0; i<G.edge.size();i++){
		Arc e = G.edge[i];
		if(e.u.nid == u) {
			ngu.vertices.push_back(e.v);
			ngu.labeluvs.push_back(e.glabel);
		}
		else if(e.v.nid == u) {
			ngu.vertices.push_back(e.u);
			ngu.labeluvs.push_back(e.glabel);
		}
	}
	return ngu;
}


PHI initializePHI(Code C, Graph G){
	string lvl0;
	lvl0=C.tuples[0].labelu;
	PHI varPHI;
	for(unsigned int i = 0;i<G.vertex.size();i++){
		if(G.vertex[i].nlabel==lvl0){
			phi varphi;
			varphi.vertices.push_back(G.vertex[i].nid);
			varPHI.phis.push_back(varphi);
		}
	}
	return varPHI;
}



vector<Node> getRightMostNodes(Code C){
	vector<Node>R;
	extendedTuple t;
	int i; 
	for(i = C.tuples.size()-1; i>=0; i--){
		t = C.tuples[i];
		if(isForwardEdge(t)) break;
	}

	Node u, v;
	v.nid = t.disv;
	v.nlabel = t.labelv;
	u.nid = t.disu;
	u.nlabel = t.labelu;
	i--;
	R.push_back(v);
	R.push_back(u);
	string temp = t.labelu;
	while(i>=0){
		t = C.tuples[i];
		if(isForwardEdge(t) && t.labelv == temp){
			temp = t.labelu;
			Node tmp;
			tmp.nid = t.disu;
			tmp.nlabel = t.labelu;
			R.push_back(tmp);
		}
		i--;
	}


	//cout<<"ERROR::getRightMostNodes is not implemented"<<endl;
	return R;
}

bool isForwardEdge(extendedTuple t){
	if(t.disu<t.disv) return true;
	return false;
}



void printPHI(PHI varPHI){
	for(unsigned int i = 0; i<varPHI.phis.size(); i++){
		cout<<"phi"<<i+1<<endl;
		for(unsigned int j = 0; j< varPHI.phis[i].vertices.size(); j++){
			cout<<varPHI.phis[i].vertices[j]<<" ";
		}
		cout<<endl;
	} 
}


bool existNode(int v,vector<Node>R){
	for(unsigned int i = 0; i<R.size(); i++){
		if(R[i].nid == v) return true;
	}
	return false;
}

bool existEdge(int ur,int v,Code C){
	for(unsigned int i = 0; i<C.tuples.size(); i++){
		if((C.tuples[i].disu == ur && C.tuples[i].disv == v) || (C.tuples[i].disv == ur && C.tuples[i].disu == v)) return true;
	}
	return false;
}

double getWeight(string labelu, string labelv, string labeluv, Graph G){
	for(unsigned int i = 0; i<G.edge.size(); i++){
		if((G.edge[i].u.nlabel == labelu && G.edge[i].v.nlabel == labelv && G.edge[i].glabel==labeluv)||(G.edge[i].u.nlabel == labelv && G.edge[i].v.nlabel == labelu && G.edge[i].glabel==labeluv))
			return G.edge[i].weight;
	}
	return -1;
}



int getOLtIndex(vector<OL>OLvec, extendedTuple t){
	for(unsigned int i = 0; i<OLvec.size(); i++){
		if(compareTuple(t,OLvec[i].t)==0) return i;
	}
	return -1;
}



bool gidExist(OLM olm, int gid){
	for(unsigned int i = 0; i<olm.gids.size(); i++){
		if(olm.gids[i]==gid) return true;
	}
	return false;
}


/**************************************************************************************************************
*********************************         Canonical Helper        *********************************************
***************************************************************************************************************/

//Compare Tuple function 
	//returns 0 if t1 = t2
	//returns -1 if t1 < t2
	//returns +1 if t1 > t2

int compareTuple(extendedTuple t1, extendedTuple t2){

	if(t1.disu == t2.disu && t1.disv == t2.disv){
		if(t1.weight > t2.weight) return -1;
		if(t1.weight < t2.weight) return 1;
 		if(t1.labelu == t2.labelu && t1.labelv == t2.labelv && t1.labeluv == t2.labeluv) return 0;
		if(t1.labelu < t2.labelu) return -1;
		if(t1.labelu > t2.labelu) return 1;
		if(t1.labelv < t2.labelv) return -1;
		if(t1.labelv > t2.labelv) return 1;
		if(t1.labeluv < t2.labeluv) return -1;
		return 1;
	}

	if(isForwardEdge(t1) && isForwardEdge(t2)){ //both are forward edge
		if(t1.disv < t2.disv) return -1;
		if(t1.disv == t2.disv && t1.disu > t2.disu) return -1;	
	}else if(!isForwardEdge(t1) && !isForwardEdge(t2)){ //both are backward edge
		if(t1.disu < t2.disu) return -1;
		if(t1.disu == t2.disu && t1.disv < t2.disv) return -1;
	}else if(isForwardEdge(t1)){
		if(t1.disv <= t2.disu) return -1;
	}else{
		if(t1.disu < t2.disv) return -1;
	}
	return 1;
}


vector<OL> getAllExtensionsWithG(Code C, Graph G){
	vector<OL>OLvec;
	vector<Node>R;
	R = getRightMostNodes(C);
	Node ur = R[0];
	if(C.tuples.size()==5){
		int tem = 10;
	}
	if(C.tuples.size()==0){
		for(unsigned int i = 0; i<G.edge.size(); i++){
			extendedTuple t;
			t.disu = 0;
			t.disv = 1;
			if(G.edge[i].u.nlabel<G.edge[i].v.nlabel){
				t.labelu = G.edge[i].u.nlabel;
				t.labelv = G.edge[i].v.nlabel;
			}else{
				t.labelu = G.edge[i].v.nlabel;
				t.labelv = G.edge[i].u.nlabel;
			}
			t.weight = G.edge[i].weight;
			t.labeluv = G.edge[i].glabel;


			int indx  = getOLtIndex(OLvec, t);
			if(indx==-1){ // ol does not exist
					indx = OLvec.size();
					OL ol;
					ol.t = t;
					OLvec.push_back(ol);
			}
		}
	}else{
	
		PHI phi_i = SubgraphIsomorphisms(C,G);
		for(unsigned int k = 0; k<phi_i.phis.size(); k++){
			phi smPhi = phi_i.phis[k];
			//Backward Edge Extension from rightmost child ur
			int phi_ur = smPhi.vertices[ur.nid];
			Neighbor nbr = getNeighbors(G,phi_ur);
			for(unsigned int l = 0; l<nbr.vertices.size(); l++){
				Node x = nbr.vertices[l];
				string labeluv = nbr.labeluvs[l];
				int v = getPhiInv(x.nid,smPhi);
				if(v!=-1 && existNode(v,R) && !existEdge(ur.nid,v,C)){
					extendedTuple t;
					t.disu = ur.nid;
					t.disv = v;
					t.labelu = ur.nlabel;
					t.labelv = x.nlabel;
					t.labeluv = labeluv;
					t.weight = getWeight(t.labelu, t.labelv, t.labeluv, G);

					//18: Add pair (t;OLt) in OLvec if not added before
					int indx  = getOLtIndex(OLvec, t);
					if(indx==-1){ // ol does not exist
						indx = OLvec.size();
						OL ol;
						ol.t = t;
						OLvec.push_back(ol);
					}

					//19: Add or modify entry OLt(qi): OLt(qi) = OLt(qi) [ Gj
					if(!gidExist(OLvec[indx].OLMvec[G.ec], G.id)) OLvec[indx].OLMvec[G.ec].gids.push_back(G.id);

				}
			}

			//Forward edge extension from rightmost path R
			for(unsigned int l = 0; l<R.size(); l++){
				int phi_u = smPhi.vertices[R[l].nid];
				Neighbor nbr_u = getNeighbors(G,phi_u);
				for(unsigned int k = 0; k<nbr_u.vertices.size(); k++){
					Node x = nbr_u.vertices[k];
					string labeluv = nbr_u.labeluvs[k];
					if(!isPhiInv(x.nid, smPhi)){
						extendedTuple t;
						t.disu = R[l].nid;
						t.disv = ur.nid+1;
						t.labelu = R[l].nlabel;
						t.labelv = x.nlabel;
						t.labeluv = labeluv;
						t.weight = getWeight(t.labelu, t.labelv,t.labeluv, G);
						//23: Add pair (t;OLt) in OLvec if not added before
						int indx  = getOLtIndex(OLvec, t);
						if(indx==-1){ // ol does not exist
							indx = OLvec.size();
							OL ol;
							ol.t = t;
							OLvec.push_back(ol);
						}

						//19: Add or modify entry OLt(qi): OLt(qi) = OLt(qi) [ Gj
						if(!gidExist(OLvec[indx].OLMvec[G.ec], G.id))  OLvec[indx].OLMvec[G.ec].gids.push_back(G.id);
					}

				}
			}
		}
	}

	return OLvec;
}


Graph getGraphFromCode(Code C){
	Graph G;
	G.ec = C.tuples.size();
	Node u;
	u.nid = C.tuples[0].disu;
	u.nlabel = C.tuples[0].labelu;
	G.vertex.push_back(u);

	for(unsigned int i = 0; i<C.tuples.size(); i++){
		Node x, y;
		extendedTuple t = C.tuples[i];

		x.nid = t.disu;
		x.nlabel = t.labelu;

		y.nid = t.disv;
		y.nlabel = t.labelv;

		if(isForwardEdge(t)){
			G.vertex.push_back(y);
		}

		Arc edg;
		edg.u = x;
		edg.v = y;
		edg.weight = t.weight;
		edg.glabel = t.labeluv;
		G.edge.push_back(edg);
	}
	G.id = 1;
	G.vc = G.vertex.size();
	return G;
}