/********************************************************************************************************************

*********************************Declaration of Functions in this file***********************************************

*********************************************************************************************************************/


char *trimwhitespace(char *str);
Graph readGraph(int id1);
gfp_pair readNextGraph(FILE *fp);
int graphCount = 1;
//int setEdgeIndex(string labelu, string labelv);
int setEdgeIndex(string labelu, string labelv, string labeluv);
sup_mws_pair MaxPws(Code C);
void preprocessData(char input[1000], char output[1000]);
void preprocessDataPart2(char inputpath[1000], char outputpath[1000], bool dflag);
bool writeGraph(char *filename2, Graph g, char* mode);
void arcfreqrecord(Arc e);
bool edgeCompare(Arc e1, Arc e2);
int getRandom(int l, int u);
double getRandomWeight(double l, double u);
Arc addWeight(Arc e);
double generateNumberWithInverseExponential(int freq);
//Graph getGraphIfInRam(int id, Graph g);
//void storeGraph(Graph g);
//int getIndexOfOldestUnusedStoredGraph();
//void initializeStoredIndicesArray();
/********************************************************************************************************************

*********************************Implementation of Functions in this file********************************************

*********************************************************************************************************************/


Graph readGraph(int id1){
	int id = id1 -1;

	Graph g;
	//g = getGraphIfInRam(id1,g);
	//if(g.id != INVALID) return g;



	fsetpos(fp, &positions[id]);
	//char str[MAXLENGTH+1];
	fgets (buf_str, MAXLENGTH, fp);
	//temp.fp = fp;
	char *pch, *context = NULL;
	char *nodeString = strtok_s(buf_str,":", &context);
	nodeString = trimwhitespace(nodeString);
	char *edgeString = strtok_s(NULL, ":", &context);
	edgeString = trimwhitespace(edgeString);
	pch = strtok_s(nodeString, " ", &context);
	while(pch != NULL){
		Node node;
		node.nid = atoi(pch);
		pch = strtok_s(NULL, " ", &context);
		node.nlabel =  string(pch);
		g.vertex.push_back(node);
		pch = strtok_s(NULL, " ", &context);

	}
	pch = strtok_s(edgeString, " ", &context);
		while(pch != NULL){
			Arc edge;
			int u = atoi(pch);
			//cout<<pch<<endl;
			pch = strtok_s(NULL, " ", &context);
			//cout<<pch<<endl;
			int v = atoi(pch);
			pch = strtok_s(NULL, " ", &context);
			edge.glabel = string(pch);
			pch = strtok_s(NULL, " ", &context);
			//cout<<pch<<endl;
			double weight = atof(pch);

			edge.u = g.getNode(u);
			edge.v = g.getNode(v);
			edge.weight = weight;
			g.edge.push_back(edge);

			pch = strtok_s(NULL, " ", &context);

		}


		g.vc = g.vertex.size();
		g.ec = g.edge.size();
		g.id = id1;


		//storeGraph(g);

		return g;
	//gfp_pair temp = readNextGraph(fp);
	//temp.g.print();
}


/*
Graph getGraphIfInRam(int id, Graph g){
	
	int indx = storedIndices[id];
	if(indx!=-1){
		if(storedGraphs[indx].accessTime != lastAccesstime) storedGraphs[indx].accessTime = ++lastAccesstime; 
			return storedGraphs[indx].g;
	}
	g.id = INVALID;
	return g;
}
*/
/*void storeGraph(Graph g){
	graphs_and_time gt;
	gt.g = g;
	gt.accessTime = ++lastAccesstime;
	if(storedGraphs.size()<MAXSTOREDGRAPH){
		storedGraphs.push_back(gt);
		storedIndices[g.id] = storedGraphs.size()-1;
	}
	else{
		int indx = getIndexOfOldestUnusedStoredGraph();
		if(indx!=-1) {
			storedIndices[storedGraphs[indx].g.id] = -1;
			storedGraphs[indx] = gt;
			storedIndices[g.id] = indx;
		}
		else cout<<"Failed to Store"<<endl;
	}
}
*/
/*int getIndexOfOldestUnusedStoredGraph(){
	int min = lastAccesstime;
	int minIndex = -1;
	for(int i = 0; i<storedGraphs.size(); i++){
		if(storedGraphs[i].accessTime<min){
			min = storedGraphs[i].accessTime;
			minIndex = i;
		}
	}
	return minIndex;
}*/

/*void initializeStoredIndicesArray(){
	for(int i =0; i<=totalGraphsInDatabase;i++){
		storedIndices[i]=-1;
	}
}*/

gfp_pair readNextGraph(FILE *fp){
	
	gfp_pair temp;

	temp.g.vc = 0;
	temp.g.ec = 0;


	//char str[MAXLENGTH+1];
	if( fgets (buf_str, MAXLENGTH, fp)!=NULL ) 
	{
		//cout<<graphCount++<<":"<<endl;
		temp.fp = fp;
		char *pch, *context = NULL;
		char *nodeString = strtok_s(buf_str,":", &context);
		nodeString = trimwhitespace(nodeString);
		char *edgeString = strtok_s(NULL, ":", &context);
		edgeString = trimwhitespace(edgeString);
		//cout<<nodeString<<endl;
		pch = strtok_s(nodeString, " ", &context);
		while(pch != NULL){
			Node node;
			node.nid = atoi(pch);
			pch = strtok_s(NULL, " ", &context);
			node.nlabel =  string(pch);
			temp.g.vertex.push_back(node);
			pch = strtok_s(NULL, " ", &context);

		}

		//cout<<edgeString<<endl;
		pch = strtok_s(edgeString, " ", &context);
		while(pch != NULL){
			Arc edge;
			int u = atoi(pch);
			//cout<<pch<<endl;
			pch = strtok_s(NULL, " ", &context);
			//cout<<pch<<endl;
			int v = atoi(pch);
			pch = strtok_s(NULL, " ", &context);
			edge.glabel = string(pch);
			pch = strtok_s(NULL, " ", &context);
			//cout<<pch<<endl;
			double weight = atof(pch);

			edge.u = temp.g.getNode(u);
			edge.v = temp.g.getNode(v);
			edge.weight = weight;
			temp.g.edge.push_back(edge);

			pch = strtok_s(NULL, " ", &context);

		}


		temp.g.vc = temp.g.vertex.size();
		temp.g.ec = temp.g.edge.size();
		temp.g.id = graphCount++;
		return temp;

	}else{
		temp.fp = fp;
		return temp;
	}

}





// Note: This function returns a pointer to a substring of the original string.
// If the given string was allocated dynamically, the caller must not overwrite
// that pointer with the returned value, since the original pointer must be
// deallocated using the same allocator with which it was allocated.  The return
// value must NOT be deallocated using free() etc.
char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}


int setEdgeIndex(string labelu, string labelv, string labeluv){
	for(unsigned int i = 0; i<EdgeList.size(); i++){
		if((EdgeList[i].labelu == labelu && EdgeList[i].labelv == labelv && EdgeList[i].labeluv == labeluv)||(EdgeList[i].labelv == labelu && EdgeList[i].labelu == labelv && EdgeList[i].labeluv == labeluv))
			return i;
	}
	EdgeMap temp;
	temp.labelu = labelu;
	temp.labelv = labelv;
	temp.labeluv = labeluv;
	EdgeList.push_back(temp);
	return EdgeList.size()-1;
}

sup_mws_pair  MaxPws(Code C){
	int support = 0;
	double weight = 0;
	double lmaxW = C.tuples[0].weight;
	int ec = C.tuples.size();
	for(unsigned int i = 0; i<C.tuples.size(); i++){
		weight += C.tuples[i].weight;
	}
	//weight /= ec; //W(C)
	double maxpws = -1;
	for(int mi = MAXEDGE; mi>0 ; mi--){
		if(C.OLMvec[mi].gids.size()>0){
			support += C.OLMvec[mi].gids.size();
			double pws = (weight + (mi-ec)*lmaxW)/mi;
			pws = pws*support;
			if(pws>maxpws) maxpws = pws;
		}
	}
	sup_mws_pair temp;
	temp.mws = maxpws;
	temp.ws = (weight/ec)*support;
	temp.mxW = support*maxW;
	return temp;

}






/*****************************************************************************************************
************************                     Preprocessing                    ************************
******************************************************************************************************/

FILE *fpxp;
bool first = true;
bool vfast = true;
int gCount = 0;
double eCount = 0;
double avgEdgeW = 0;
double maxWhere = -100;
double minWhere = 100;

void preprocessData(char input[1000], char output[1000]){
	char inputpath1[1000];
	strcpy_s(inputpath1, input);
	strcat_s(inputpath1,"\\active\\output.txt");
	char outputpath1[1000];
	strcpy_s(outputpath1, output);
	strcat_s(outputpath1,"_active.txt");
	preprocessDataPart2(inputpath1, outputpath1, false);

	for(int i = 0; i<freqlist.size(); i++){
		/*if(freqlist[i].freq>100000) freqlist[i].e.weight = getRandomWeight(0.3,0.4);
		else if(freqlist[i].freq>15000) freqlist[i].e.weight = getRandomWeight(0.4,0.65);
		else if(freqlist[i].freq>10000) freqlist[i].e.weight = getRandomWeight(0.5,0.75);
		else if(freqlist[i].freq>1000) freqlist[i].e.weight = getRandomWeight(0.6,0.85);
		if(freqlist[i].freq<=1000) freqlist[i].e.weight = getRandomWeight(0.65,0.95);*/

		freqlist[i].e.weight = generateNumberWithInverseExponential(freqlist[i].freq);
		avgEdgeW = freqlist[i].e.weight*freqlist[i].freq;
		if(maxWhere < freqlist[i].e.weight) maxWhere = freqlist[i].e.weight;
		if(minWhere > freqlist[i].e.weight) minWhere = freqlist[i].e.weight;
		cout<<freqlist[i].e.u.nlabel<<","<<freqlist[i].e.v.nlabel<<","<<freqlist[i].e.glabel<<"::::"<<freqlist[i].freq<<"---"<<freqlist[i].e.weight<<endl;
	}
	preprocessDataPart2(inputpath1, outputpath1, true);
	fclose(fpxp);
	avgEdgeW = avgEdgeW/eCount;
	eCount = eCount/gCount;
	cout<<"#graphs: "<<gCount<<"\n#avgEdge: "<<eCount<<endl;
	cout<<"averageEdgeWeight: "<<avgEdgeW<<endl;
	cout<<"MaxW: "<<maxWhere<<"    MinW: "<<minWhere<<endl;
}



void preprocessDataPart2(char inputpath[1000], char outputpath[1000], bool dflag){
	errno_t err;
	FILE *pf;
	err	= fopen_s(&pf, inputpath, "r");
	if(err!=0){
		cout<<"ERROR::Dataset cannot be opened. Please ensure path is correct"<<endl;
		cout<<inputpath<<endl;
		return;
	}
	gCount = 0;
	Graph gwrite;
	gwrite.vc = 0;
	while( fgets (buf_str, MAXLENGTH, pf)!=NULL ) {
		if(buf_str[0]=='t'){
		//new graph
			if( gwrite.vc >0  ){
				//write the graph to output path
				if(vfast && dflag){
					cout<<"Hi"<<endl;
					gCount++;
					writeGraph(outputpath,gwrite, "w");
					vfast = false;
				}else if(dflag){
					gCount++;
					writeGraph(outputpath,gwrite, "a");
				}
				//gwrite.print();
			}

			char *pch, *context = NULL;
			//char *nodeString = strtok_s(buf_str,"", &context);
			char *nodeString = trimwhitespace(buf_str);
			//cout<<nodeString<<endl;
			pch = strtok_s(nodeString, " ", &context);
			pch = strtok_s(NULL, " ", &context);
			pch = strtok_s(NULL, " ", &context);
			//node.nlabel =  string(pch);

			Graph gtem;
			gtem.id = atoi(pch);
			gtem.vc = 0;
			gtem.ec = 0;
			gwrite = gtem;
		}else if(buf_str[0]=='v'){
		//vertex
			char *pch, *context = NULL;
			//char *nodeString = strtok_s(buf_str,"", &context);
			char *nodeString = trimwhitespace(buf_str);
			//cout<<nodeString<<endl;
			pch = strtok_s(nodeString, " ", &context);
			pch = strtok_s(NULL, " ", &context);
			Node node;
			node.nid = atoi(pch);
			pch = strtok_s(NULL, " ", &context);
			node.nlabel =  string(pch);
			//cout<<node.nid<<"::"<<node.nlabel<<endl;
			gwrite.vc++;
			gwrite.vertex.push_back(node);
			
		}else if(buf_str[0]=='e'){
		//edge
			char *pch, *context = NULL;
			//char *nodeString = strtok_s(buf_str,"", &context);
			char *edgeString = trimwhitespace(buf_str);
			//cout<<nodeString<<endl;
			pch = strtok_s(edgeString, " ", &context);
			pch = strtok_s(NULL, " ", &context);
			Arc edg;
			edg.u = gwrite.getNode(atoi(pch));
			pch = strtok_s(NULL, " ", &context);
			edg.v = gwrite.getNode(atoi(pch));
			pch = strtok_s(NULL, " ", &context);
			edg.glabel = pch;
			//node.nlabel =  string(pch);
			//assign weight;
			edg.weight = atoi(pch);
			
			if(!dflag) arcfreqrecord(edg);
			else{
				edg = addWeight(edg);
			}
			gwrite.edge.push_back(edg);
		}
	}
	fclose(pf);
}


Arc addWeight(Arc e){
	for(int i = 0; i<freqlist.size(); i++){
		if(edgeCompare(e,freqlist[i].e)) e.weight = freqlist[i].e.weight;
	}
	return e;
}


bool writeGraph(char *filename2, Graph g, char* mode){
	FILE *fpx;
	errno_t err;
	//g.print();
	//cout<<filename2<<endl;
	eCount += g.edge.size();
	if(first){
			err	= fopen_s(&fpx, filename2,mode);
			if(err!=0) return false;
			fpxp = fpx;
			first = false;
		}
		
		//fprintf_s(fp,"%d %d %d ", gid++, g.vertex.size(), g.edge.size());
		
		for(unsigned int i = 0; i<g.vertex.size(); i++){
			fprintf_s(fpxp,"%d %s ", g.vertex[i].nid, g.vertex[i].nlabel.c_str());
		}
		fprintf_s(fpxp,": ");
		for(unsigned int i = 0; i<g.edge.size(); i++){
			fprintf_s(fpxp,"%d %d %s %.2lf ", g.edge[i].u.nid, g.edge[i].v.nid,g.edge[i].glabel.c_str(), g.edge[i].weight);
		}
		fprintf(fpxp,"\n");
		//fclose(fp);
		return true;
}


void arcfreqrecord(Arc e){
	bool found = false;
	for(int i = 0; i<freqlist.size(); i++){
		if(edgeCompare(freqlist[i].e, e)){
			freqlist[i].freq++;
			found = true;
			break;
		}
	}
	if(!found){
		edge_freq ef;
		ef.e = e;
		ef.freq = 1;
		freqlist.push_back(ef);
	}
}


bool edgeCompare(Arc e1, Arc e2){
	if(e1.u.nlabel == e2.u.nlabel && e1.v.nlabel == e2.v.nlabel && e1.glabel == e2.glabel) return true;
	if(e1.u.nlabel == e2.v.nlabel && e1.v.nlabel == e2.u.nlabel && e1.glabel == e2.glabel) return true;
	return false;
}






int getRandom(int l, int u){
	int k = u-l;
    return rand() % k + l;
}



double getRandomWeight(double l, double u){

	int low = l*100, up = u * 100; //precision upto two decimal place
	double k = getRandom(low, up);
	k = k/100;
	return k;
}


double generateNumberWithInverseExponential(int freq){
	/*double res =0;
	for(int i =0; i<freq; i++){
		double decoy = getRandomWeight(0,1);
		decoy = log(1-decoy)/(6);
		res += decoy;
	}*/
	int k = 100 * ((exp(-0.0006*freq)+0.3*getRandomWeight(0.2,1))/1.3);
	double kd = (k*1.0)/100;
	return kd;
}
